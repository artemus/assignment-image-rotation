#include "image.h"


/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_BITS_ERROR,
    WRITE_PADDING_ERROR,
    /* коды других ошибок  */
};

enum write_status to_bmp( FILE* out, struct image const* img );
