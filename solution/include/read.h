#include "image.h"

enum read_status  {
    READ_OK = 0,
    READ_INVALID,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
    /* коды других ошибок  */
};

enum read_status from_bmp( FILE* in, struct image* img );


