#include "read.h"
#include "write.h"

enum open_status  {
    OPEN_OK = 0,
    OPEN_READ_ERROR,
    OPEN_WRONG_TYPE,
    OPEN_WRITE_ERROR,
};

enum open_status open_file(char *filename, int mode, struct image* img );
