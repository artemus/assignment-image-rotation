#include "../include/open.h"
#include "../include/rotate.h"

enum open_status open_file(char *filename, int mode, struct image* img) {
    if (mode == 0) {
        FILE *bmp_file = fopen(filename, "rb");
        if (bmp_file == NULL) {
            return OPEN_WRONG_TYPE;
        }
        struct image original_image = {0};
        enum read_status status = from_bmp(bmp_file, &original_image);
        if (status != READ_OK) {
            return OPEN_READ_ERROR;
        }
        *img = rotate(&original_image);
        free_image(&original_image);
        fclose(bmp_file);
    } else {
        FILE *bmp_file = fopen(filename, "wb");
        enum write_status status = to_bmp(bmp_file, img);
        if (status != WRITE_OK) {
            return OPEN_WRITE_ERROR;
        }
        free_image(img);
        fclose(bmp_file);
    }
    return OPEN_OK;
}
