#include "../include/read.h"
#include <stdlib.h>

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};


enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header bmpHeader = {0};
    fread(&bmpHeader, sizeof(struct bmp_header), 1, in);

    if (bmpHeader.bfType != 0x4D42) {
        return READ_INVALID_HEADER;
    }

    uint32_t padding = bmpHeader.biWidth % 4;

    img->height = bmpHeader.biHeight;
    img->width = bmpHeader.biWidth;
    img->data = malloc(img->width * img->height * sizeof(struct pixel));

    for (size_t i = 0; i < img->height; i++) {
        for (size_t j = 0; j < img->width; j++) {
            fread(&img->data[i * img->width + j], sizeof(struct pixel), 1, in);
        }
        fseek(in, padding, SEEK_CUR);
    }

    return READ_OK;

}
