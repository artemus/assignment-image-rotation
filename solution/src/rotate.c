#include "../include/image.h"
#include <stdlib.h>

struct image rotate(struct image const *in) {
    struct image rotated = {
            .width = in->height,
            .height = in->width,
            .data = malloc(sizeof(struct pixel) * in->width * in->height)
    };
    for (size_t j = 0; j < in->width; j++) {
        for (size_t i = 0; i < in->height; i++) {
            rotated.data[j * rotated.width + i] = in->data[(in->height - i - 1) * in->width + j];
        }
    }
    return rotated;
}

void free_image(struct image *image) {
    free(image->data);
}
