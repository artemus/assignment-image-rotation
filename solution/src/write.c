#include "../include/write.h"

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

enum write_status to_bmp(FILE *out, struct image const *img) {
    struct bmp_header bmpHeader = {
            .bfType = 0x4D42,
            .bfileSize = sizeof(struct pixel) * img->height * img->width +
                         sizeof(struct bmp_header) + img->height * (img->width % 4),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage =  sizeof(struct pixel) * img->height * img->width + (img->width % 4) * img->height,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0,
    };
    uint32_t padding = img->width % 4;
    uint64_t add = 0;

    if (fwrite(&bmpHeader, sizeof(struct bmp_header), 1, out) != 1) return WRITE_ERROR;
    for (size_t i = 0; i < img->height; i++) {
        if (fwrite(&(img->data[i * img->width]), sizeof(struct pixel), img->width, out) != img->width) {
            return WRITE_BITS_ERROR;
        }
        if (fwrite(&add, 1, padding, out) != padding) {
            return WRITE_PADDING_ERROR;
        }
    }
    return WRITE_OK;
}
