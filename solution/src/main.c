#include "../include/open.h"


int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning

    struct image main_image = {0};

    char *input_image = argv[1];
    char *output_image = argv[2];

    enum open_status read_open_status = open_file(input_image, 0, &main_image);
    if (read_open_status != OPEN_OK) {
        printf("Произошла ошибка в чтении");
    }
    enum open_status write_open_status = open_file(output_image, 1, &main_image);
    if (write_open_status != OPEN_OK) {
        printf("Произошла ошибка в записи");
    }
    return 0;
}
